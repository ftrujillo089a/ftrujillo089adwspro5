<?php
        // put your code here
        include 'config.php'; 
          ?>
<!DOCTYPE html>
<!--
Mantenimiento de provincias y sus poblaciones 
-->
     
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/proyecto1.css">
        <title>Provincias Españolas</title>
    </head>
    <body>
        <h1 id="titulo"><?=Config::TITULO?></h1>
        <ul>
            <li>Opciones:</li>
            <li><a href="gestion_provincia.php">Gestión Provincias</a></li>
            <li><a href="gestion_poblacion.php">Gestión Problaciones</a></li>
        </ul>
            
        <ul>
            <li>Documentación por tema:</li> 
              <li><a href="documentacion/DWST5PROYECTO5.pdf">Tema5 POO.Enunciado</a></li>
        </ul>
            
           <ul>
               <li>Documentación de este proyecto:</li>
               <li><a href="documentacion/proyecto1.pdf">Documentación</a></li>
           </ul>     
            
        </ul>
        <div id="pie"><?=Config::AUTOR?> <?=date("d/m/Y", FECHA);?> <?=Config::EMPRESA?> <?=Config::CURSO?></div>                    
        
    </body>
</html>
