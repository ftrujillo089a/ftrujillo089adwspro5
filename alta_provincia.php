<?php

/* 
 */
 include_once("Provincia.php");
 //print_r($_REQUEST);
function recoge($campo) {
               if (isset($_REQUEST[$campo])) {
              
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            };
            return $valor;
        }        
        
        function grabar($obj_provincia){
            $file = fopen("provincias.txt", "a");
            $linea = $obj_provincia->getCodigo() . ";" . $obj_provincia->getNominacion() .  ";" .
                    $obj_provincia->getSuperficie() . ";" . $obj_provincia->getHabitantes() . ";" .
                    $obj_provincia->getComunidad() . "\r\n";
            fwrite($file, $linea);           
            fclose($file);
        }
  // Crear objeto provincia
        
        $codigo=recoge('codigo');
        $nominacion=recoge('nominacion');
        $superficie=recoge('superficie');
        $habitantes=recoge('habitantes');
        $comunidad=recoge('comunidad');  
       
        $obj_provincia=new Provincia($codigo, $nominacion, $superficie, $habitantes, $comunidad);
                
   // Grabar en fichero   
   grabar($obj_provincia);
   header("Location:".$_SERVER['HTTP_REFERER']);  // volver página interior
?>