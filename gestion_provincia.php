<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
    include 'config.php';
    include_once("Provincia.php"); 
                
?>
<html>
    <head>
        <title>Provincias Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::TITULO?></h1>
     <div>GESTIÓN de PROVINCIAS</div><br>
    
    <h>Listado:</h>
    <table> 
       <tr>
	<th>Código</th>
	<th>Nombre</th>
	<th>Superficie</th>
	<th>Habitantes</th>
        <th>Comunidad</th>

    </tr>
    <?php
      $file = fopen("provincias.txt", "r");
          
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
         
           $array_datos=  explode(';', $linea);
        
           $obj_provincia=new Provincia($array_datos[0], $array_datos[1], $array_datos[2],
                   $array_datos[3], $array_datos[4]);
       
          ?>   
                <tr>
                    <td><?=$obj_provincia->getCodigo()?></td>
                    <td><?=$obj_provincia->getNominacion()?></td>
                    <td><?=$obj_provincia->getSuperficie()?></td>
                    <td><?=$obj_provincia->getHabitantes()?></td>
                    <td><?=$obj_provincia->getComunidad()?></td>
                 </tr>                                                    
     <?php
          }
            fclose($file);
    ?>
       </table>
    <a  id='inicio' href='index.php'>Inicio</a>
    <a href="form_provincia.php">Altas</a><br>
    <div id="pie"><?=Config::AUTOR?> <?=date("d/m/Y", FECHA);?> <?=Config::EMPRESA?> <?=Config::CURSO?></div>    
    </body>
</html>